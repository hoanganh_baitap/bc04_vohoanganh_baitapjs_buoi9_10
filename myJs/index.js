
var nvArr = [];

const DSNV = "DSNV";

var nvArrJson = localStorage.getItem("DSNV");

if (nvArrJson) {
    nvArr = JSON.parse(nvArrJson);
    for (var i = 0; i < nvArr.length; i++) {
        var nv = nvArr[i];
        nvArr[i] = new NhanVien(
            nv.taiKhoan,
            nv.hoTen,
            nv.email,
            nv.matKhau,
            nv.ngayLam,
            nv.luongCoBan,
            nv.chucVu,
            nv.gioLamTrongThang,
        )
    }
    inDanhSach(nvArr);
}

function handleAddButton() {
    var header = document.getElementById("header-title");
    header.innerText = "Log In";
    document.getElementById("tknv").disabled = false;
    document.getElementById("btnThemNV").disabled = false;
    // reset
    resetForm();
    resetInput();
}

function handleAccountValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid =
        validator.kiemTraRong(newNv.taiKhoan, "tbTKNV", "Nhập tài khoản")
        && validator.kiemTraDoDai(newNv.taiKhoan, "tbTKNV", "Từ 4 đến 6 kí tự", 4, 6);
}

function handleNameValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid =
        validator.kiemTraRong(newNv.hoTen, "tbTen", "Nhập họ tên")
        && validator.kiemTraKhongPhaiSo(newNv.hoTen, "tbTen", "Tên ko chứa kí tự số")
}

function handleMailValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid =
        validator.kiemTraRong(newNv.email, "tbEmail", "Nhập email")
        && validator.kiemTraEmail(newNv.email, "tbEmail", "Email không hợp lệ")
}

function handlePassWordValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid =
        validator.kiemTraRong(newNv.matKhau, "tbMatKhau", "Nhập mật khẩu")
        && validator.kiemTraMatKhau(newNv.matKhau, "tbMatKhau", "Mật khẩu gồm 6-10 kí tự, có ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt")
}

function handleSalaryValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid =
        validator.kiemTraRong(newNv.luongCoBan, "tbLuongCB", "Nhập lương")
        && validator.kiemTraMinMax(newNv.luongCoBan, "tbLuongCB", "Lương từ 1 triệu đến 20 triệu", 1000000, 20000000);
}

function handleOfficeValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid =
        validator.kiemTraChucVu(newNv.chucVu, "tbChucVu", "Chọn chức vụ");
}

function handleWorkHourValidation() {
    var newNv = layThongTinTuForm();

    var isValid = null;
    isValid = validator.kiemTraRong(newNv.gioLamTrongThang, "tbGiolam", "Nhập giờ làm")
        && validator.kiemTraMinMax(newNv.gioLamTrongThang, "tbGiolam", "Giờ làm từ 80-200 giờ", 80, 200)
}


// thêm nhân viên có kiểm tra validate
function themNhanVien() {
    var newNv = layThongTinTuForm();

    var isValid;

    isValid =
        validator.kiemTraRong(newNv.taiKhoan, "tbTKNV", "Nhập tài khoản")
        && validator.kiemTraDoDai(newNv.taiKhoan, "tbTKNV", "Từ 4 đến 6 kí tự", 4, 6);
     
    isValid &=
        validator.kiemTraRong(newNv.hoTen, "tbTen", "Nhập họ tên")
        && validator.kiemTraKhongPhaiSo(newNv.hoTen, "tbTen", "Tên ko chứa kí tự số");

    isValid &=
        validator.kiemTraRong(newNv.email, "tbEmail", "Nhập email")
        && validator.kiemTraEmail(newNv.email, "tbEmail", "Email không hợp lệ");

    isValid &=
        validator.kiemTraRong(newNv.matKhau, "tbMatKhau", "Nhập mật khẩu")
        && validator.kiemTraMatKhau(newNv.matKhau, "tbMatKhau", "Mật khẩu gồm 6-10 kí tự, có ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt");

    isValid &=
        validator.kiemTraRong(newNv.ngayLam, "tbNgay", "Nhập ngày")
        && validator.kiemTraNgay(newNv.ngayLam, "tbNgay", "Tháng/ ngày/ năm");

    isValid &=
        validator.kiemTraRong(newNv.luongCoBan, "tbLuongCB", "Nhập lương")
        && validator.kiemTraMinMax(newNv.luongCoBan, "tbLuongCB", "Lương từ 1 triệu đến 20 triệu", 1000000, 20000000);

    isValid &= validator.kiemTraChucVu(newNv.chucVu, "tbChucVu", "Chọn chức vụ");
    
    isValid &= 
        validator.kiemTraRong(newNv.gioLamTrongThang, "tbGiolam", "Nhập giờ làm")
        && validator.kiemTraMinMax(newNv.gioLamTrongThang, "tbGiolam", "Giờ làm từ 80-200 giờ", 80, 200);

    if (!isValid) {
        return;
    }
    nvArr.push(newNv);
    var nvArrJson = JSON.stringify(nvArr);
    localStorage.setItem("DSNV", nvArrJson);
    inDanhSach(nvArr);


    resetForm();
}


// xóa nhân viên
function xoaNhanVien(taiKhoanCanXoa) {

    for (var i = 0; i < nvArr.length; i++) {
        if (nvArr[i].taiKhoan == taiKhoanCanXoa) {
            nvArr.splice(i, 1);
        }
    }
    var nvArrJson = JSON.stringify(nvArr);
    localStorage.setItem("DSNV", nvArrJson);

    inDanhSach(nvArr);
}


// sửa thông tin nhân viên
function suaNhanVien(taiKhoanCanSua) {
    resetInput();

    var header = document.getElementById('header-title');
    header.innerText = "Sửa nhân viên";
    document.getElementById('tknv').disabled = true;
    document.getElementById('btnThemNV').disabled = true;

    var nvEdit = null;
    nvArr.forEach((nv) => {
        if (nv.taiKhoan == taiKhoanCanSua) {
            nvEdit = nv;
        }
    })
    dienThongTinLenForm(nvEdit.taiKhoan, nvEdit.hoTen, nvEdit.email, nvEdit.matKhau, nvEdit.ngayLam, nvEdit.luongCoBan, nvEdit.chucVu, nvEdit.gioLamTrongThang);

}

// cập nhật thông tin nhân viên có validate
function capNhat() {
    // lấy thông tin từ form và tạo thành object mới
    var newNv = layThongTinTuForm();
    var header = document.getElementById('header-title');
    if (header.innerText == "Log In") {

        return;
    }

    var isValid = null;
    isValid =
        validator.kiemTraRong(newNv.hoTen, 'tbTen', "Nhập họ tên")
        && validator.kiemTraKhongPhaiSo(newNv.hoTen, 'tbTen', "Tên ko dc chứa kí tự số");

    isValid &=
        validator.kiemTraRong(newNv.email, 'tbEmail', "Nhập email")
        && validator.kiemTraEmail(newNv.email, 'tbEmail', "Email không hợp lệ");
  
    isValid &=
        validator.kiemTraRong(newNv.matKhau, 'tbMatKhau', "Nhập mật khẩu")
        && validator.kiemTraMatKhau(newNv.matKhau, 'tbMatKhau', "Mật khẩu chứa từ 6-10 kí tự, chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt");

    isValid &=
        validator.kiemTraRong(newNv.ngayLam, 'tbNgay', "Nhập ngày")
        && validator.kiemTraNgay(newNv.ngayLam, 'tbNgay', "Nhập đúng định dạng tháng/ ngày/ năm");

    isValid &=
        validator.kiemTraRong(newNv.luongCoBan, 'tbLuongCB', "Nhập lương")
        && validator.kiemTraMinMax(newNv.luongCoBan, 'tbLuongCB', "Lương từ 1 triệu đến 20 triệu", 1000000, 20000000);

    isValid &=
        validator.kiemTraChucVu(newNv.chucVu, 'tbChucVu', "Chọn chức vụ");

    isValid &= 
        validator.kiemTraRong(newNv.gioLamTrongThang, 'tbGiolam', 'Nhập giờ làm')
        && validator.kiemTraMinMax(newNv.gioLamTrongThang, 'tbGiolam', 'Giờ làm từ 80-200 giờ', 80, 200);
    if (!isValid) {
        return;
    }

    document.getElementById('tknv').disabled = false;
    document.getElementById('btnThemNV').disabled = false;
    for (var i = 0; i < nvArr.length; i++) {
        if (nvArr[i].taiKhoan == newNv.taiKhoan) {
            nvArr[i] = newNv; //* thay object cũ = object mới

        }
    }
    var nvArrJson = JSON.stringify(nvArr);
    localStorage.setItem("DSNV", nvArrJson);
    // render ra màn hình
    inDanhSach(nvArr);

    resetForm();

}

// tìm nhân viên qua search xếp loại
function timNhanVien() {

    var arrSearch = [];
    var input = document.getElementById('searchName');
    var value = input.value;
    for (var i = 0; i < nvArr.length; i++) {

        if (nvArr[i].xepLoai().includes(value)) {
            arrSearch.push(nvArr[i]);
        }


    }
    // console.log('arrSearch: ', arrSearch);
    // render ra table
    inDanhSach(arrSearch);
}






