// lưu các constructor
function NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLamTrongThang) {
    this.taiKhoan = taiKhoan;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.chucVu = chucVu;
    this.gioLamTrongThang = gioLamTrongThang;
    this.tongLuong = () => {
        switch (this.chucVu) {
            case "Sếp": {
                return +this.luongCoBan * 3;
            }
            case "Trưởng phòng": {
                return +this.luongCoBan * 2;
            }
            case "Nhân viên": {
                return +this.luongCoBan;
            }
            default: {
                return "Không";
            }
        }
    }
    this.xepLoai = () => {
        var gioLam = this.gioLamTrongThang * 1;

        if (gioLam >= 192) {
            return "xuất sắc";
        } else if (gioLam >= 176) {
            return "giỏi";
        } else if (gioLam >= 160) {
            return "khá";
        } else {
            return "trung bình"
        }
    }
}